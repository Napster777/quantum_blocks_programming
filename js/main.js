window.addEventListener("load", function(){
    var draggable = document.getElementById("dragthis"),
        droppable = document.getElementById("dropthis");

    draggable.addEventListener("dragstart", function () {
        droppable.classList.add("hint");
    });

    droppable.addEventListener("dragenter", function () {
        droppable.classList.add("active");
    });

    droppable.addEventListener("dragleave", function () {
        droppable.classList.remove("active");
    });

    droppable.addEventListener("dragover", function (evt) {
        evt.preventDefault();
    });

    droppable.addEventListener("drop", function (evt) {
        evt.preventDefault();
        alert("def c_amod15(a, power):\n" +
            "    \"\"\"Controlled multiplication by a mod 15\"\"\"\n" +
            "    if a not in [2,7,8,11,13]:\n" +
            "        raise ValueError(\"'a' must be 2,7,8,11 or 13\")\n" +
            "    U = QuantumCircuit(4)        \n" +
            "    for iteration in range(power):\n" +
            "        if a in [2,13]:\n" +
            "            U.swap(0,1)\n" +
            "            U.swap(1,2)\n" +
            "            U.swap(2,3)\n" +
            "        if a in [7,8]:\n" +
            "            U.swap(2,3)\n" +
            "            U.swap(1,2)\n" +
            "            U.swap(0,1)\n" +
            "        if a == 11:\n" +
            "            U.swap(1,3)\n" +
            "            U.swap(0,2)\n" +
            "        if a in [7,11,13]:\n" +
            "            for q in range(4):\n" +
            "                U.x(q)\n" +
            "    U = U.to_gate()\n" +
            "    U.name = \"%i^%i mod 15\" % (a, power)\n" +
            "    c_U = U.control()\n" +
            "    return c_U");
    });

    draggable.addEventListener("dragend", function () {
        droppable.classList.remove("hint");
        droppable.classList.remove("active");
    });

    // draggable.addEventListener("drag", function (evt) {  });
});